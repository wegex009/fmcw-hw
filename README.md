Make sure you lock any files you are working on, by opening gitlab.com, selecting the file you want to edit,
and press the lock button on the upper right of the window.
